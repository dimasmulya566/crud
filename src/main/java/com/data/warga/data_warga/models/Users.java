package com.data.warga.data_warga.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(	name = "users",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
@Data
@NoArgsConstructor
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Column(name = "username")
    @Size(max = 20)
    private String username;

    @NotBlank
    @Size(max = 20)
    @Column(name = "first_name")
    private String first_name;

    @NotBlank
    @Size(max = 20)
    @Column(name = "last_name")
    private String last_name;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(max = 120)
    private String password;

    public Users(Integer id,String username,
                 String firstName,String lastName,String email, String password) {
        this.id = id;
        this.username = username;
        this.first_name = firstName;
        this.last_name = lastName;
        this.email = email;
        this.password = password;
    }
}
