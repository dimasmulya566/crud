package com.data.warga.data_warga.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData<T> {

    private boolean status;
    private String messageValidation;
    private List<String> messages = new ArrayList<>();
    private T payload;

}
