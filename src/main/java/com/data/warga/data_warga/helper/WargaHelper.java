package com.data.warga.data_warga.helper;

import com.data.warga.data_warga.components.Constant;
import com.data.warga.data_warga.dao.WargaDao;
import com.data.warga.data_warga.dto.WargaRequest;
import com.data.warga.data_warga.dto.WargaResponse;
import com.data.warga.data_warga.models.Warga;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class WargaHelper {

    @Autowired
    private WargaDao wargaDao;
    @Autowired
    private ModelMapper mapper;

    public Warga convertToEntity(WargaRequest request){
        return mapper.map(request, Warga.class);
    }

    public Map<String, Object> getStringObjectMap(WargaRequest wargaRequest) {
        Map<String, Object> result = new HashMap<>();
        var dataWarga = convertToEntity(wargaRequest);
        if(dataWarga.getNoTelepon() == null ||
                dataWarga.getNoTelepon().equalsIgnoreCase("")){
            result.put(Constant.MESSAGE_STRING, "No hp wajib diisi");
        } else if(dataWarga.getIdCard() == null ||
                dataWarga.getIdCard().equalsIgnoreCase("")){
            result.put(Constant.MESSAGE_STRING, "No KTP tidak Boleh kosong");
        }else {
            dataWarga.setDeleted(Boolean.FALSE);
            Warga warga = wargaDao.save(dataWarga);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put("Data", warga);
        }

        return  result;
    }
}
