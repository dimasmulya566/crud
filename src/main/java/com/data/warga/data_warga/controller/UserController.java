package com.data.warga.data_warga.controller;


import com.data.warga.data_warga.components.constant.DataBaseConstant;
import com.data.warga.data_warga.components.endpoint.UserEndpoint;
import com.data.warga.data_warga.dto.UserRequest;
import com.data.warga.data_warga.models.Users;
import com.data.warga.data_warga.service.UsersService;
import com.data.warga.data_warga.util.ResponseData;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import static com.data.warga.data_warga.Constants.*;
import static com.data.warga.data_warga.components.constant.DataBaseConstant.*;
import static com.data.warga.data_warga.components.endpoint.UserEndpoint.*;

@RestController
@RequestMapping(value = user)
public class UserController {
    @Autowired
    private UsersService userDetailsService;

    @RequestMapping(value = register, method = RequestMethod.POST)
    public ResponseEntity<ResponseData<Object>> register(@Valid @RequestBody UserRequest users, Errors errors) {
        ResponseData<Object> responseData = new ResponseData<>();
        if (errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()){
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setMessageValidation(messageError);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);

        }   else {
            responseData.setStatus(true);
            responseData.setMessageValidation(messageDone);
            responseData.setPayload(jwtToken(userDetailsService.register(users)));

        }
        return ResponseEntity.ok(responseData);
    }

    @RequestMapping(value = UserEndpoint.login, method = RequestMethod.POST)
    public ResponseEntity<Map<String,Object>> login(@Valid @RequestBody Map<String,Object> objectMap, Errors errors) {
        String email = (String) objectMap.get("email");
        String password = (String) objectMap.get("password");
        Map<String,Object> map = new HashMap<>();
        if (errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()){
                map.put("message",error.getDefaultMessage());
            }
            map.put("status",false);
            map.put("payload",null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(map);

        }   else {
            map.put("status",true);
            map.put("payload",jwtToken(userDetailsService.validateUser(email,password)));

        }
        return ResponseEntity.ok(map);
    }

    private Map<String,String> jwtToken(Users users){
        long Timestamp = System.currentTimeMillis();
        String token = Jwts.builder().signWith(SignatureAlgorithm.HS256,API_SECRET_KEY)
                .setIssuedAt(new Date(Timestamp))
                .setExpiration(new Date(Timestamp + TOKEN_VALIDITY))
                .claim("id",users.getId())
                .claim("username",users.getUsername())
                .claim("first_name",users.getFirst_name())
                .claim("last_name",users.getLast_name())
                .claim("email",users.getEmail())
                .claim("password",users.getPassword())
                .compact();
        Map<String,String> map = new HashMap<>();
        map.put("token",token);
        map.put("message", DataBaseConstant.login);
        return map;
    }
}

