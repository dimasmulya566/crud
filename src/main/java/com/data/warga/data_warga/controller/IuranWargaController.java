package com.data.warga.data_warga.controller;

import com.data.warga.data_warga.components.endpoint.IuranWargaEndPoint;
import com.data.warga.data_warga.dto.IuranWargaRequest;
import com.data.warga.data_warga.service.IuranWargaService;
import com.data.warga.data_warga.util.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

import static com.data.warga.data_warga.components.constant.DataBaseConstant.*;

@RestController
@RequestMapping(value = IuranWargaEndPoint.path)
@CrossOrigin(origins = "http://localhost:8081")
public class IuranWargaController {

    @Autowired
    private IuranWargaService service;

    @GetMapping(value = IuranWargaEndPoint.path)
    @CrossOrigin(origins = "http://localhost:8081")
    public Map<String, Object> getIuranWargaResponses(){
        return service.getIuranWargaResponses();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @CrossOrigin(origins = "http://localhost:8081")
    public ResponseEntity<ResponseData<Object>> create(@Valid @RequestParam("data_warga_id") Long id,
                                                       @RequestBody IuranWargaRequest request) {
        ResponseData<Object> responseData = new ResponseData<>();
        try {
            responseData.setStatus(true);
            responseData.setPayload(service.createIuran(id, request));
            responseData.setMessageValidation(messageDone);
            return ResponseEntity.ok(responseData);
        } catch (Exception ex) {
            responseData.setStatus(false);
            responseData.setMessageValidation(messageError);
            responseData.getMessages().add("ERROR");
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

    }

    @DeleteMapping(value = IuranWargaEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public void delete (@PathVariable("id") Long id){
        service.delete(id);
    }

    @PutMapping(value = IuranWargaEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public ResponseEntity<Object> updateDataAlamat(@PathVariable("id") Long id,
                                                   @RequestBody IuranWargaRequest request) {
        return new ResponseEntity<>(service.updateBayaran(id, request), HttpStatus.OK);
    }

    @GetMapping(value = IuranWargaEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public Map<String, Object> getById(@RequestParam("id") Long id) {
        return service.getById(id);
    }
}
