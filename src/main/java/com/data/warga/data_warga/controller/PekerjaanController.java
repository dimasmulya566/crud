package com.data.warga.data_warga.controller;

import com.data.warga.data_warga.components.endpoint.PekerjaanEndPoint;
import com.data.warga.data_warga.dto.PekerjaanRequest;
import com.data.warga.data_warga.dto.PekerjaanResponse;
import com.data.warga.data_warga.service.PekerjaanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = PekerjaanEndPoint.path)
@CrossOrigin(origins = "http://localhost:8081")
public class PekerjaanController {

    @Autowired
    private PekerjaanService service;

    @GetMapping(value = PekerjaanEndPoint.path)
    @CrossOrigin(origins = "http://localhost:8081")
    public List<PekerjaanResponse> findAll(){
        return service.findAll();
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @CrossOrigin(origins = "http://localhost:8081")
    public Map<String, Object> create(@RequestParam("data_warga_id") Long id,
                                      @RequestBody PekerjaanRequest request){
        return service.create(id,request);
    }

    @DeleteMapping(value = PekerjaanEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public void delete (@PathVariable("id") Long id){
        service.delete(id);
    }

    @PutMapping(value = PekerjaanEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public ResponseEntity<Object> updateDataPekerjaan(@PathVariable("id") Long id,
                                                   @RequestBody PekerjaanRequest request) {
        return new ResponseEntity<>(service.update(id, request), HttpStatus.OK);
    }
    @GetMapping(value = PekerjaanEndPoint.pathURL)
    @CrossOrigin(origins = "http://localhost:8081")
    public Map<String, Object> getById(@RequestParam("id") Long id) {
        return service.getById(id);
    }
}
