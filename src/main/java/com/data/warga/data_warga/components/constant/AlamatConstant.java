package com.data.warga.data_warga.components.constant;

public class AlamatConstant {
    public static final String emptyAlamat = "Alamat tidak boleh kosong";
    public static final String emptyKelurahan = "Kelurahan tidak boleh kosong";
    public static final String emptyKecamatan = "Kecamatan tidak boleh kosong";
    public static final String emptyKota = "Kota tidak boleh kosong";
    public static final String emptyKodePos = "Kode Pos tidak boleh kosong";

    public static final String nullAlamat = "Alamat Harus di isi dan tidak boleh kosong";
    public static final String nullKelurahan = "Kelurahan Harus di isi dan tidak boleh kosong";
    public static final String nullKecamatan = "Kecamatan Harus di isi dan tidak boleh kosong";
    public static final String nullKota = "Kota Harus di isi dan tidak boleh kosong";
    public static final String nullKodePos = "Kode Pos Harus di isi dan tidak boleh kosong";
}
