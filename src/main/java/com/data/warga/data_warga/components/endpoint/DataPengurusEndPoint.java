package com.data.warga.data_warga.components.endpoint;

public final class DataPengurusEndPoint {
    public static final String pathVariableId = "id";
    public static final String path = "/DataPengurus";
    public static final String pathURL = "/{" + pathVariableId + "}";
    public static final String pathVariableSize = "size";
    public static final String pathVariablePage = "page";
}
