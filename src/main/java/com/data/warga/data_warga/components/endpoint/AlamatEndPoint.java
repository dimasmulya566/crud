package com.data.warga.data_warga.components.endpoint;

public final class AlamatEndPoint {
    public static final String pathVariableId = "id";
    public static final String path = "/tampilkan/dataAlamatWarga";
    public static final String pathURL = "/{" + pathVariableId + "}";
}
