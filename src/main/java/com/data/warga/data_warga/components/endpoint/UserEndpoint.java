package com.data.warga.data_warga.components.endpoint;

public class UserEndpoint {
    public static final String user = "/user";
    public static final String register = "/register";
    public static final String login = "/login";
}
