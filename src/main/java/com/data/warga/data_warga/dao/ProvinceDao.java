package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.models.area.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProvinceDao extends JpaRepository<Province,Integer> {
}
