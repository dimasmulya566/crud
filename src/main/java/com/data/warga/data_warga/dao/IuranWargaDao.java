package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.models.IuranWarga;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IuranWargaDao extends JpaRepository<IuranWarga, Long> {
}
