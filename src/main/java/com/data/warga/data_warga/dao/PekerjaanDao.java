package com.data.warga.data_warga.dao;

import com.data.warga.data_warga.models.Pekerjaan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PekerjaanDao extends JpaRepository<Pekerjaan, Long> {
}
