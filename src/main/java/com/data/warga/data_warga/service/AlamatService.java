package com.data.warga.data_warga.service;

import com.data.warga.data_warga.dto.AlamatRequest;
import com.data.warga.data_warga.dto.AlamatResponse;
import com.data.warga.data_warga.models.Alamat;

import java.util.List;
import java.util.Map;

public interface AlamatService {

    public List<AlamatResponse> findAll();
    public Map<String, Object> create(Integer province_id,Integer city_id,Integer kec_id,
                                      Integer kel_id, Integer zipcode,Long wargaId,
                                      AlamatRequest request);
    public void delete(Long id);
    public Map<String, Object> getById(Long id);
    public Map<String, Object> update(Integer province_id,Integer city_id,Integer kec_id,
                                      Integer kel_id, Integer zipcode,Long id,
                                      AlamatRequest request);

}
