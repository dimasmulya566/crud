package com.data.warga.data_warga.service;

import com.data.warga.data_warga.components.Constant;
import com.data.warga.data_warga.dao.*;
import com.data.warga.data_warga.dto.AlamatRequest;
import com.data.warga.data_warga.dto.AlamatResponse;
import com.data.warga.data_warga.helper.SimulateProcess;
import com.data.warga.data_warga.models.Alamat;
import com.data.warga.data_warga.models.area.Zipcode;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
@CacheConfig(cacheNames = "alamatCache")
public class AlamatServiceImpl implements AlamatService{

    @Autowired
    private AlamatDao alamatDao;

    @Autowired
    private WargaDao wargaDao;

    @Autowired
    private ProvinceDao provinceDao;

    @Autowired
    private KecamatanDao kecamatanDao;
    @Autowired
    private KotaDao kotaDao;
    @Autowired
    private KelurahanDao kelurahanDao;
    @Autowired
    private ZIpcodeDao zIpcodeDao;

    @Autowired
    private SimulateProcess simulatedProsess;

    @Autowired
    private ModelMapper mapper;

    public Alamat convertToEntity(AlamatRequest request){
        return mapper.map(request, Alamat.class);
    }

    public AlamatResponse convertToResponse(Alamat alamat){
        return mapper.map(alamat, AlamatResponse.class);
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    @Cacheable(cacheNames = "alamatCache")
    @Override
    public List<AlamatResponse> findAll() {
        simulatedProsess.simulatedProsess();
        var alamat = alamatDao.findAll().stream().filter(alamatWarga -> alamatWarga.getDeleted()
                .equals(Boolean.FALSE)).collect(Collectors.toList());
        log.info("error", alamat);
        return alamat.stream().map(this::convertToResponse).collect(Collectors.toList());
    }

    @CacheEvict(cacheNames = "alamatCache", allEntries = true)
    @Override
    public Map<String, Object> create(Integer province_id,Integer city_id,Integer kec_id,
                                      Integer kel_id, Integer zipcode,Long wargaId,
                                      AlamatRequest request){
        Map<String, Object> result = new HashMap<>();
        var getData = wargaDao.findById(wargaId);
        var getProvince = provinceDao.findById(province_id);
        var getKota = kotaDao.findById(city_id);
        var getKecamatan = kecamatanDao.findById(kec_id);
        var getKelurahan = kelurahanDao.findById(kel_id);
        var getZipcode = zIpcodeDao.findById(zipcode);
        simulatedProsess.simulatedProsess();
        var setAlamatWarga = convertToEntity(request);
        if (getData.isPresent()) {
            setAlamatWarga.setDeleted(Boolean.FALSE);
            setAlamatWarga.setProvince_id(getProvince.get());
            setAlamatWarga.setWarga(getData.get());
            setAlamatWarga.setCity_id(getKota.get());
            setAlamatWarga.setKec_id(getKecamatan.get());
            setAlamatWarga.setKel_id(getKelurahan.get());
            setAlamatWarga.setZipcode(getZipcode.get());
            Alamat resultInput = alamatDao.save(setAlamatWarga);

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
            return result;
        }
        result.put(Constant.RESPONSE_STRING, HttpStatus.BAD_REQUEST);
        result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        return result;
    }

    @Caching(evict = { @CacheEvict(cacheNames = "alamat", key = "#id"),
            @CacheEvict(cacheNames = "alamatCache", allEntries = true) })
    @Override
    public void delete(Long id) {
        simulatedProsess.simulatedProsess();
        var delete = alamatDao.findById(id).stream().filter(alamat ->
                alamat.getDeleted().equals(Boolean.FALSE)).findFirst();
        if(delete.isPresent()){
            delete.get().setDeleted(Boolean.TRUE);
            alamatDao.save(delete.get());
        }
        delete.orElseThrow();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Cacheable(cacheNames = "alamatCache", key = "#id", unless = "#result == null")
    @Override
    public Map<String, Object> getById(Long id) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        try {
            var data = alamatDao.findById(id);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(data.get()));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @CacheEvict(cacheNames = "alamatCache", key = "#id", allEntries = true)
    @Override
    public Map<String, Object> update(Integer province_id,Integer city_id,Integer kec_id,
                                      Integer kel_id, Integer zipcode,Long id,
                                      AlamatRequest request) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        try {
            var data = alamatDao.findById(id);
            var getProvince = provinceDao.findById(province_id);
            var getKota = kotaDao.findById(city_id);
            var getKecamatan = kecamatanDao.findById(kec_id);
            var getKelurahan = kelurahanDao.findById(kel_id);
            var getZipcode = zIpcodeDao.findById(zipcode);
            data.get().setAlamat(request.getAlamat());
            data.get().setKec_id(getKecamatan.get());
            data.get().setKel_id(getKelurahan.get());
            data.get().setCity_id(getKota.get());
            data.get().setZipcode(getZipcode.get());
            data.get().setZipcode(getZipcode.get());

            Alamat resultInput = alamatDao.save(data.get());

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }


}
