package com.data.warga.data_warga.service;

import com.data.warga.data_warga.components.Constant;
import com.data.warga.data_warga.dao.DataPengurusDao;
import com.data.warga.data_warga.dto.DataPengurusRequest;
import com.data.warga.data_warga.dto.DataPengurusResponse;
import com.data.warga.data_warga.helper.SimulateProcess;
import com.data.warga.data_warga.models.DataPengurus;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataPengurusServiceImpl implements DataPengurusService{

    @Autowired
    private DataPengurusDao dao;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private SimulateProcess simulatedProsess;

    public DataPengurus convertToEntity(DataPengurusRequest request){
        return mapper.map(request, DataPengurus.class);
    }
    public DataPengurusResponse convertToResponse(DataPengurus dataPengurus){
        return mapper.map(dataPengurus, DataPengurusResponse.class);
    }

    @SneakyThrows
    @Override
    public Map<String, Object> create(DataPengurusRequest request) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        var dataPengurus = convertToEntity(request);
        if(dataPengurus.getUmur() == null ||
                dataPengurus.getUmur().equalsIgnoreCase("")){
            result.put(Constant.MESSAGE_STRING, "Umur Harus di Isi");
        } else if(dataPengurus.getMassaKerja() == null ||
                dataPengurus.getMassaKerja().equalsIgnoreCase("")){
            result.put(Constant.MESSAGE_STRING, "Massa Kerja tidak Boleh kosong");
        }else {
            dataPengurus.setDeleted(Boolean.FALSE);
            DataPengurus pengurus = dao.save(dataPengurus);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put("Data", pengurus);
        }
        return result;
    }

    @Override
    public Map<String, Object> getDataPengurusResponses(){
        Map<String, Object> result = new HashMap<>();
        List<DataPengurusResponse> responses = new ArrayList<>();
        simulatedProsess.simulatedProsess();
        for(DataPengurus dataPengurus : dao.findAll()){
            DataPengurusResponse dataPengurusResponse = convertToResponse(dataPengurus);
            responses.add(dataPengurusResponse);
        }

        String message;

        if (responses.isEmpty()){
            message = "Data Kosong";
            result.put(Constant.MESSAGE_STRING, message);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        } else {
            message = "Tampilkan Data";
            result.put(Constant.MESSAGE_STRING, message);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.DATA_STRING, responses);
            result.put(Constant.TOTAL_STRING,responses.size());
        }
        return result;
    }

    @Override
    public void delete(Long id) {
        simulatedProsess.simulatedProsess();
        var delete = dao.findById(id).stream().filter(alamat ->
            alamat.getDeleted().equals(Boolean.FALSE)).findFirst();
        if(delete.isPresent()){
            delete.get().setDeleted(Boolean.TRUE);
            dao.save(delete.get());
        }
        delete.orElseThrow();
    }

    @SneakyThrows
    @Override
    public Map<String, Object> update(Long id, DataPengurusRequest request) {
        simulatedProsess.simulatedProsess();
        Map<String, Object> result = new HashMap<>();
        try {
            var data = dao.findById(id);
            data.get().setNama(request.getNama());
            data.get().setJabatan(request.getJabatan());
            data.get().setJurusan(request.getJurusan());
            data.get().setMassaKerja(request.getMassaKerja());
            data.get().setPendidikan(request.getPendidikan());
            data.get().setStatus(request.getStatus());
            data.get().setUmur(request.getUmur());

            DataPengurus resultInput = dao.save(data.get());

            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(resultInput));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Override
    public Map<String, Object> getById(Long id) {
        Map<String, Object> result = new HashMap<>();
        simulatedProsess.simulatedProsess();
        try {
            var data = dao.findById(id);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.SUCCESS_STRING);
            result.put(Constant.DATA_STRING, convertToResponse(data.get()));
        } catch (Exception e) {
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.MESSAGE_STRING, Constant.ID_NOT_FOUND);
        }
        return result;
    }

    @Override
    public DataPengurus create(DataPengurus user) {
        simulatedProsess.simulatedProsess();
        DataPengurus userSave = dao.save(user);
        return userSave;
    }

    @Override
    public Map<String, Object> findByNama(String nama, Pageable pageable){
        Map<String, Object> result = new HashMap<>();
        List<DataPengurusResponse> responses = new ArrayList<>();
        simulatedProsess.simulatedProsess();
        for(DataPengurus dataPengurus : dao.findByNamaContains(nama, pageable)){
            DataPengurusResponse dataPengurusResponse = convertToResponse(dataPengurus);
            responses.add(dataPengurusResponse);
        }

        String message;

        if (responses.isEmpty()){
            message = "Data Kosong";
            result.put(Constant.MESSAGE_STRING, message);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
        } else {
            message = "Tampilkan Data";
            result.put(Constant.MESSAGE_STRING, message);
            result.put(Constant.RESPONSE_STRING, HttpStatus.OK);
            result.put(Constant.DATA_STRING, responses);
            result.put(Constant.TOTAL_STRING,responses.size());
        }
        return result;
    }

    public Iterable<DataPengurus> saveDataPengurus(Iterable<DataPengurus> DataPengurus){
        simulatedProsess.simulatedProsess();
        return dao.saveAll(DataPengurus);
    }

}
