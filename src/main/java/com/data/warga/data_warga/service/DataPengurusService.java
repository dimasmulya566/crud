package com.data.warga.data_warga.service;

import com.data.warga.data_warga.dto.DataPengurusRequest;
import com.data.warga.data_warga.models.DataPengurus;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface DataPengurusService {
    Map<String, Object> create(DataPengurusRequest request);
    Map<String, Object> getDataPengurusResponses();
    void delete(Long id);
    Map<String, Object> update(Long id, DataPengurusRequest request);
    Map<String, Object> getById(Long id);
    Map<String, Object> findByNama(String nama, Pageable pageable);
    Iterable<DataPengurus> saveDataPengurus(Iterable<DataPengurus> DataPengurus);
    DataPengurus create(DataPengurus user);
}
