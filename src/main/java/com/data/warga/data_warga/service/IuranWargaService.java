package com.data.warga.data_warga.service;

import com.data.warga.data_warga.dto.IuranWargaRequest;
import com.data.warga.data_warga.dto.IuranWargaResponse;

import java.util.List;
import java.util.Map;

public interface IuranWargaService {
    Map<String, Object> getIuranWargaResponses();
    Map<String, Object> createIuran(Long wargaId, IuranWargaRequest request);
    void delete(Long id);
    Map<String, Object> getById(Long id);
    Map<String, Object> updateBayaran(Long id, IuranWargaRequest request);
}
