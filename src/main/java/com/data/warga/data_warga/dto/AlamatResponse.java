package com.data.warga.data_warga.dto;

import com.data.warga.data_warga.models.Warga;
import com.data.warga.data_warga.models.area.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AlamatResponse {

    private Long id;
    private String nama;
    private String alamat;
    private String kel_name;
    private String kec_name;
    private String cityName;
    private String zipcode;
    private String province_name;
}

