package com.data.warga.data_warga.dto;

import com.data.warga.data_warga.models.Alamat;
import com.data.warga.data_warga.Enum.Shift;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class SatpamResponse {
    private Long id;
    private String nama;
    private Integer noTelepon;
    @Enumerated(EnumType.STRING)
    private Shift shift;
    private Alamat alamat;
}
