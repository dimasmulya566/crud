package com.data.warga.data_warga.dto;

import com.data.warga.data_warga.Enum.DataPengurusEnum;
import com.data.warga.data_warga.Enum.JurusanPendidikan;
import com.data.warga.data_warga.Enum.Pendidikan;
import com.data.warga.data_warga.Enum.Status;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;

import static com.data.warga.data_warga.components.constant.DataPengurusConstant.*;

@Data
public class DataPengurusRequest {
    private String nama;

    @Enumerated(EnumType.STRING)
    private DataPengurusEnum jabatan;

    @NotEmpty(message = emptyUmur)
    private String umur;

    @Enumerated(EnumType.STRING)
    private Pendidikan pendidikan;

    @Enumerated(EnumType.STRING)
    private JurusanPendidikan jurusan;

    @NotEmpty(message = emptyMassaKerja)
    @DateTimeFormat(pattern = "dd-MM-YYYY")
    private String massaKerja;

    @Enumerated(EnumType.STRING)
    private Status status;
}
