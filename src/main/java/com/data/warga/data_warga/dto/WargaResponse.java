package com.data.warga.data_warga.dto;

import com.data.warga.data_warga.Enum.Agama;
import com.data.warga.data_warga.Enum.Hubungan;
import com.data.warga.data_warga.Enum.JenisKelamin;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WargaResponse {

    private Long id;
    private String noTelepon;
    private String noKartuKeluarga;
    private String idCard;
    private String nama;
    private String tempatTanggalLahir;
    @Enumerated(EnumType.STRING)
    private Agama agama;
    @Enumerated(EnumType.STRING)
    private Hubungan hubungan;
    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

}
