package com.data.warga.data_warga.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;


@Data
@Builder
public class IuranWargaRequest {
    @JsonIgnore
    private Long id;
    @NotEmpty(message = "Harus Bayar")
    @NotNull(message = "Harus Bayar")
    private BigDecimal iuranSampah;
    @NotEmpty(message = "Harus Bayar")
    private BigDecimal iuranKebersihan;
    @NotEmpty(message = "Harus Bayar")
    private BigDecimal iuranKeamanan;
    @NotEmpty(message = "Harus Bayar")
    private BigDecimal iuranKasRT;
    @NotEmpty(message = "Harus Bayar")
    private BigDecimal iuranKasRW;
}
