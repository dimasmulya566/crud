package com.data.warga.data_warga.dto;

import com.data.warga.data_warga.Enum.Agama;
import com.data.warga.data_warga.Enum.Hubungan;
import com.data.warga.data_warga.Enum.JenisKelamin;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static com.data.warga.data_warga.components.constant.WargaConstant.*;

@Data
public class WargaRequest {

    @JsonIgnore
    private Long id;
    private String noTelepon;
    @NotEmpty(message = emptyNoKartuKeluarga)
    @NotNull(message = nullNoKartuKeluarga)
    private String noKartuKeluarga;
    private String idCard;
    @NotEmpty(message = emptyNama)
    @NotNull(message = nullNama)
    private String nama;
    @NotEmpty(message = emptyTempatTanggalLahir)
    @NotNull(message = nullTempatTanggalLahir)
    private String tempatTanggalLahir;
    @Enumerated(EnumType.STRING)
    private Agama agama;
    @Enumerated(EnumType.STRING)
    private Hubungan hubungan;
    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

}
